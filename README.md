## Como rodar o projeto?
Instale o `DOCKER` na maquina.<br>
Acesse o diretório do projeto.<br>
Rode o comando `docker-compose up`<br>

## Como rodar as migrations?
Acesse o diretório do projeto.<br>
Rode o comando:<br>
`docker exec -it contraktor-api_webapp_1 npm run migrate`<br>

PS: Caso o nome do container esteja diferente basta alterar o `contraktor-api_webapp_1`
