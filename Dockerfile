FROM node:alpine

# Create app directory
RUN mkdir -p /usr/src/app
RUN chmod -R 777 /usr/src/app

WORKDIR /usr/src/app

# Copy code
COPY package.json /usr/src/app/
COPY . /usr/src/app


RUN apk update && apk upgrade && \
    apk add --no-cache bash git openssh

RUN npm install -g knex
RUN npm install -g typescript
RUN npm install -g ts-node
RUN npm install
RUN npm install --dotenv-extended
RUN npm run build

# Run config
EXPOSE		3010
CMD ["npm", "run", "start"]
