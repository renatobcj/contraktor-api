"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const bodyParser = require("body-parser");
require("reflect-metadata");
const inversify_express_utils_1 = require("inversify-express-utils");
const inversify_1 = require("inversify");
const contracts_service_1 = require("./modules/contracts/contracts.service");
const contracts_repository_1 = require("./modules/contracts/contracts.repository");
const db_config_1 = require("./database/db.config");
require("./modules/contracts/contracts.controller");
require("./modules/parties/parties.controller");
const files_repository_1 = require("./modules/files/files.repository");
const parties_repository_1 = require("./modules/parties/parties.repository");
const files_service_1 = require("./modules/files/files.service");
const parties_service_1 = require("./modules/parties/parties.service");
const cors = require("cors");
const express = require("express");
class App {
    constructor(appInit) {
        this.setContainer();
        this.init();
        this.port = appInit.port;
    }
    setContainer() {
        let container = new inversify_1.Container();
        container.bind("ContractsService").to(contracts_service_1.ContractsService);
        container.bind("ContractsRepository").to(contracts_repository_1.ContractsRepository);
        container.bind("FilesService").to(files_service_1.FilesService);
        container.bind("FilesRepository").to(files_repository_1.FilesRepository);
        container.bind("PartiesService").to(parties_service_1.PartiesService);
        container.bind("PartiesRepository").to(parties_repository_1.PartiesRepository);
        container.bind("DBConnection").toConstantValue(db_config_1.default);
        this.container = container;
    }
    init() {
        let server = new inversify_express_utils_1.InversifyExpressServer(this.container);
        server.setConfig((app) => {
            app.use(cors());
            app.use(bodyParser.urlencoded({ extended: true }));
            app.use(bodyParser.json());
            app.use('/uploads', express.static('uploads'));
        });
        server.setErrorConfig((app) => {
            app.use((err, req, res, next) => {
                res.status(200).json({ error: err.message });
            });
        });
        this.app = server.build();
    }
    listen() {
        this.app.listen(this.port, () => {
            console.log(`App listening on the http://localhost:${this.port}`);
        });
    }
}
exports.default = App;
//# sourceMappingURL=app.js.map