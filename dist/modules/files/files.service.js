"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FilesService = void 0;
const inversify_1 = require("inversify");
let FilesService = class FilesService {
    constructor(filesRepository) {
        this.filesRepository = filesRepository;
    }
    createFile(file) {
        return __awaiter(this, void 0, void 0, function* () {
            const FILE_TYPES = {
                "application/pdf": "PDF",
                "application/msword": "DOC"
            };
            // formata os tipos de arquivos aceitos em uma string
            const acceptedTypes = Object.keys(FILE_TYPES).map(key => FILE_TYPES[key]).join(", ");
            const mimeType = file.mimetype;
            if (!(mimeType in FILE_TYPES))
                throw new Error(`Tipo de arquivo inválido, formatos aceitos: ${acceptedTypes}`);
            const fileToSave = { tipo: FILE_TYPES[mimeType], arquivo: file.path, bytes: file.size };
            const createdFileId = yield this.filesRepository.createFile(fileToSave);
            return yield this.filesRepository.findFile({ id: createdFileId });
        });
    }
    findFileByContractId(contractId) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.filesRepository.findFileByContractId(contractId);
        });
    }
};
FilesService = __decorate([
    inversify_1.injectable(),
    __param(0, inversify_1.inject("FilesRepository"))
], FilesService);
exports.FilesService = FilesService;
//# sourceMappingURL=files.service.js.map