"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ContractsController = void 0;
const inversify_express_utils_1 = require("inversify-express-utils");
const inversify_1 = require("inversify");
const createContract_dto_1 = require("./dto/createContract.dto");
const uploadFile_middleware_1 = require("../../middlewares/uploadFile.middleware");
const dto_validator_1 = require("../../validators/dto.validator");
const contractPart_dto_1 = require("./dto/contractPart.dto");
let ContractsController = class ContractsController {
    constructor(contractsService) {
        this.contractsService = contractsService;
    }
    create(req, createContractDto) {
        return __awaiter(this, void 0, void 0, function* () {
            const contractFile = req.file;
            createContractDto = yield dto_validator_1.validateDto(createContractDto, createContract_dto_1.CreateContractDto);
            return yield this.contractsService.createContract(contractFile, createContractDto);
        });
    }
    findAll() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.contractsService.findAllContracts();
        });
    }
    findOne(contractId) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.contractsService.findContract(contractId);
        });
    }
    delete(contractId) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.contractsService.deleteContract(contractId);
        });
    }
    addPartToContract(contractPartDto) {
        return __awaiter(this, void 0, void 0, function* () {
            contractPartDto = yield dto_validator_1.validateDto(contractPartDto, contractPart_dto_1.ContractPartDto);
            return yield this.contractsService.addPartToContract(contractPartDto);
        });
    }
    removePartFromContract(contractPartDto) {
        return __awaiter(this, void 0, void 0, function* () {
            contractPartDto = yield dto_validator_1.validateDto(contractPartDto, contractPart_dto_1.ContractPartDto);
            return yield this.contractsService.removePartFromContract(contractPartDto);
        });
    }
};
__decorate([
    inversify_express_utils_1.httpPost("/", uploadFile_middleware_1.uploadMiddleware.single('file')),
    __param(0, inversify_express_utils_1.request()), __param(1, inversify_express_utils_1.requestBody())
], ContractsController.prototype, "create", null);
__decorate([
    inversify_express_utils_1.httpGet("/")
], ContractsController.prototype, "findAll", null);
__decorate([
    inversify_express_utils_1.httpGet("/:id"),
    __param(0, inversify_express_utils_1.requestParam("id"))
], ContractsController.prototype, "findOne", null);
__decorate([
    inversify_express_utils_1.httpDelete("/:id"),
    __param(0, inversify_express_utils_1.requestParam("id"))
], ContractsController.prototype, "delete", null);
__decorate([
    inversify_express_utils_1.httpPost("/part"),
    __param(0, inversify_express_utils_1.requestBody())
], ContractsController.prototype, "addPartToContract", null);
__decorate([
    inversify_express_utils_1.httpPatch("/part"),
    __param(0, inversify_express_utils_1.requestBody())
], ContractsController.prototype, "removePartFromContract", null);
ContractsController = __decorate([
    inversify_express_utils_1.controller("/contracts"),
    __param(0, inversify_1.inject("ContractsService"))
], ContractsController);
exports.ContractsController = ContractsController;
//# sourceMappingURL=contracts.controller.js.map