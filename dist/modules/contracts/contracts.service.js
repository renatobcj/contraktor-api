"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ContractsService = void 0;
const inversify_1 = require("inversify");
let ContractsService = class ContractsService {
    constructor(filesService, partiesService, contractsRepository) {
        this.filesService = filesService;
        this.partiesService = partiesService;
        this.contractsRepository = contractsRepository;
    }
    createContract(contractFile, createContractDto) {
        return __awaiter(this, void 0, void 0, function* () {
            //adicionaria verificao das datas de vigencia aqui, por exemplo se a vigencia_fim pode ser no passado, etc...
            const createdFile = yield this.filesService.createFile(contractFile);
            createContractDto.arquivo_id = createdFile.id;
            const createdContractId = yield this.contractsRepository.createContract(createContractDto);
            return yield this.findContract(createdContractId);
        });
    }
    findAllContracts() {
        return __awaiter(this, void 0, void 0, function* () {
            const contracts = yield this.contractsRepository.findAllContracts();
            if (contracts.length === 0)
                return contracts;
            yield Promise.all(contracts.map((contract, i) => __awaiter(this, void 0, void 0, function* () {
                contracts[i].arquivo = yield this.filesService.findFileByContractId(contract.id);
                contracts[i].partes = yield this.partiesService.findPartiesByContractId(contract.id);
            })));
            return contracts;
        });
    }
    findContract(contractId) {
        return __awaiter(this, void 0, void 0, function* () {
            const contract = yield this.contractsRepository.findContract(contractId);
            if (!contract)
                throw new Error("Contrato não encontrado");
            contract.arquivo = yield this.filesService.findFileByContractId(contract.id);
            contract.partes = yield this.partiesService.findPartiesByContractId(contract.id);
            return contract;
        });
    }
    deleteContract(contractId) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.contractsRepository.deleteContract(contractId);
            return true;
        });
    }
    addPartToContract(contractPartDto) {
        return __awaiter(this, void 0, void 0, function* () {
            const contract = yield this.findContract(contractPartDto.contrato_id);
            const alreadyPart = contract.partes.filter((parte) => parte.id === contractPartDto.parte_id);
            if (alreadyPart.length > 0)
                throw new Error("Já é uma parte do contrato");
            const partInfo = yield this.partiesService.findPart(contractPartDto.parte_id);
            if (!partInfo)
                throw new Error("Parte não cadastrada");
            yield this.contractsRepository.addPartToContract(contractPartDto);
            return true;
        });
    }
    removePartFromContract(contractPartDto) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.contractsRepository.removePartFromContract(contractPartDto);
            return true;
        });
    }
};
ContractsService = __decorate([
    inversify_1.injectable(),
    __param(0, inversify_1.inject("FilesService")),
    __param(1, inversify_1.inject("PartiesService")),
    __param(2, inversify_1.inject("ContractsRepository"))
], ContractsService);
exports.ContractsService = ContractsService;
//# sourceMappingURL=contracts.service.js.map