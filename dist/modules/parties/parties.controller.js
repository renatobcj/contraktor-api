"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PartiesController = void 0;
const inversify_express_utils_1 = require("inversify-express-utils");
const create_part_dto_1 = require("./dto/create-part.dto");
const dto_validator_1 = require("../../validators/dto.validator");
const inversify_1 = require("inversify");
let PartiesController = class PartiesController {
    constructor(partiesService) {
        this.partiesService = partiesService;
    }
    createPart(createPartDto) {
        return __awaiter(this, void 0, void 0, function* () {
            createPartDto = yield dto_validator_1.validateDto(createPartDto, create_part_dto_1.CreatePartDto);
            return yield this.partiesService.createPart(createPartDto);
        });
    }
    findAllParties() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.partiesService.findAllParties();
        });
    }
    findOnePart(partId) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.partiesService.findPart(partId);
        });
    }
};
__decorate([
    inversify_express_utils_1.httpPost("/"),
    __param(0, inversify_express_utils_1.requestBody())
], PartiesController.prototype, "createPart", null);
__decorate([
    inversify_express_utils_1.httpGet("/")
], PartiesController.prototype, "findAllParties", null);
__decorate([
    inversify_express_utils_1.httpGet("/:id"),
    __param(0, inversify_express_utils_1.requestParam("id"))
], PartiesController.prototype, "findOnePart", null);
PartiesController = __decorate([
    inversify_express_utils_1.controller("/parties"),
    __param(0, inversify_1.inject("PartiesService"))
], PartiesController);
exports.PartiesController = PartiesController;
//# sourceMappingURL=parties.controller.js.map