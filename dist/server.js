"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require('dotenv').config();
const app_1 = require("./app");
const app = new app_1.default({ port: process.env.APP_PORT });
app.listen();
//# sourceMappingURL=server.js.map