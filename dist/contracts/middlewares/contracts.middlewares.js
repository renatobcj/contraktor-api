"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const multer = require("multer");
exports.uploadMiddleware = multer({ dest: 'public/images', limits: { 'fieldSize': 52428800 } }); //50mb de limite
//# sourceMappingURL=contracts.middlewares.js.map