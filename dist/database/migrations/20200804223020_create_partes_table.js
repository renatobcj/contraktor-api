"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.down = exports.up = void 0;
function up(knex) {
    return __awaiter(this, void 0, void 0, function* () {
        yield knex.raw("CREATE TABLE `partes` ( `id` INT(11) NOT NULL AUTO_INCREMENT, `nome` VARCHAR(50) NULL COLLATE 'latin1_swedish_ci', `sobrenome` VARCHAR(50) NULL COLLATE 'latin1_swedish_ci', `email` VARCHAR(100) NULL COLLATE 'latin1_swedish_ci', `cpf` VARCHAR(15) NULL COLLATE 'latin1_swedish_ci', `telefone` VARCHAR(15) NULL COLLATE 'latin1_swedish_ci', `criado_em` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP, PRIMARY KEY (`id`) USING BTREE ) COLLATE='latin1_swedish_ci' ENGINE=InnoDB AUTO_INCREMENT=17 ;");
    });
}
exports.up = up;
function down(knex) {
    return __awaiter(this, void 0, void 0, function* () {
        knex.raw("DROP TABLE `partes`;");
    });
}
exports.down = down;
//# sourceMappingURL=20200804223020_create_partes_table.js.map