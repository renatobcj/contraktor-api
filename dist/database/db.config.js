"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Knex = require("knex");
const knexConfig = require('../../knexfile');
let knex = Knex(knexConfig);
exports.default = knex;
//# sourceMappingURL=db.config.js.map