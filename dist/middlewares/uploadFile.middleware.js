"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.uploadMiddleware = void 0;
const multer = require("multer");
const path = require("path");
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './uploads');
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + path.extname(file.originalname)); //Appending extension
    }
});
exports.uploadMiddleware = multer({
    storage: storage,
    limits: {
        'fieldSize': 52428800 //50mb de limite
    }
});
//# sourceMappingURL=uploadFile.middleware.js.map