import {plainToClass} from "class-transformer";
import {validate, validateOrReject} from "class-validator";
import {ClassType} from "class-transformer/ClassTransformer";

//helper para validar o dto usando class-validator
export const validateDto = async <T>(object: object, classInstance: ClassType<T>): Promise<T> => {
    const objectToClass = plainToClass(classInstance, object);

    try {
        await validateOrReject(objectToClass);
        return objectToClass;
    } catch (e) {
        const errors = e.map((error) => (
            `${error.property}: ${Object.keys(error.constraints).map((key) => error.constraints[key]).join(", ")}`
        )).join("\n");

        throw new Error(errors);
    }
}