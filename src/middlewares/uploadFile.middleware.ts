import * as multer from 'multer';
import * as path from 'path';

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './uploads')
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + path.extname(file.originalname)) //Appending extension
    }
})

export const uploadMiddleware = multer({
    storage: storage,
    limits: {
        'fieldSize': 52428800  //50mb de limite
    }
})