import * as bodyParser from 'body-parser';
import "reflect-metadata";
import {InversifyExpressServer} from "inversify-express-utils";
import {Container} from "inversify";
import {ContractsService} from "./modules/contracts/contracts.service";
import {ContractsRepository} from "./modules/contracts/contracts.repository";
import knex from "./database/db.config";
import * as Knex from "knex";
import "./modules/contracts/contracts.controller";
import "./modules/parties/parties.controller";
import {FilesRepository} from "./modules/files/files.repository";
import {PartiesRepository} from "./modules/parties/parties.repository";
import {FilesService} from "./modules/files/files.service";
import {PartiesService} from "./modules/parties/parties.service";
import * as cors from 'cors';
import * as express from "express";


class App {
    public app;
    public port: string
    private container: Container;

    constructor(appInit: { port: string }) {
        this.setContainer();
        this.init();
        this.port = appInit.port
    }

    setContainer() {
        let container = new Container();

        container.bind<ContractsService>("ContractsService").to(ContractsService);
        container.bind<ContractsRepository>("ContractsRepository").to(ContractsRepository);

        container.bind<FilesService>("FilesService").to(FilesService);
        container.bind<FilesRepository>("FilesRepository").to(FilesRepository);

        container.bind<PartiesService>("PartiesService").to(PartiesService);
        container.bind<PartiesRepository>("PartiesRepository").to(PartiesRepository);

        container.bind<Knex>("DBConnection").toConstantValue(knex);

        this.container = container;
    }

    init() {
        let server = new InversifyExpressServer(this.container);

        server.setConfig((app) => {
            app.use(cors())
            app.use(bodyParser.urlencoded({extended: true}));
            app.use(bodyParser.json());
            app.use('/uploads', express.static('uploads'));
        });

        server.setErrorConfig((app) => {
            app.use((err, req, res, next) => {
                res.status(200).json({error: err.message});
            });
        })

        this.app = server.build();
    }


    public listen() {
        this.app.listen(this.port, () => {
            console.log(`App listening on the http://localhost:${this.port}`)
        })
    }
}

export default App