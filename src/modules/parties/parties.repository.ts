import {inject, injectable} from "inversify";
import * as Knex from "knex";
import {IPart} from "./interfaces/part.interface";
import {SearchPartOptions} from "./interfaces/search-part.interface";

@injectable()
export class PartiesRepository {
    constructor(@inject("DBConnection") private db: Knex) {
    }

    async findPartiesByContractId(contractId: number): Promise<IPart[]> {
        return await this.db('partes')
            .innerJoin('contratos_partes', 'partes.id', 'contratos_partes.parte_id')
            .where('contratos_partes.contrato_id', contractId)
            .select('partes.*');
    };

    async findPart(partId: number): Promise<IPart> {
        return await this.db("partes")
            .where({id: partId})
            .select("*")
            .first();
    }

    async findAllParties(filterOptions: SearchPartOptions): Promise<IPart[]> {
        const query = this.db("partes");

        if (filterOptions) {
            query.where(filterOptions);
        }

        return await query.select("*");

    }

    async createPart(createPartDto): Promise<number> {
        return await this.db("partes").insert(createPartDto);
    }
}