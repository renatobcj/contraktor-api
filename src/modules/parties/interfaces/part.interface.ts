export interface IPart {
    id: number,
    nome: string;
    sobrenome: string;
    email: string;
    cpf: string;
    telefone: string;
}