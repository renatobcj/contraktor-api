export interface SearchPartOptions {
    cpf?: string;
    nome?: string;
    sobrenome?: string;
}