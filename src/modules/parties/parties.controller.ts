import {controller, httpGet, httpPost, requestBody, requestParam, response} from "inversify-express-utils";
import * as express from "express";
import {CreatePartDto} from "./dto/create-part.dto";
import {validateDto} from "../../validators/dto.validator";
import {IPart} from "./interfaces/part.interface";
import {inject} from "inversify";
import {PartiesService} from "./parties.service";

@controller("/parties")
export class PartiesController {
    constructor(
        @inject("PartiesService") private partiesService: PartiesService
    ) {
    }

    @httpPost("/")
    async createPart(@requestBody() createPartDto: CreatePartDto): Promise<IPart> {
        createPartDto = await validateDto(createPartDto, CreatePartDto);
        return await this.partiesService.createPart(createPartDto);
    }

    @httpGet("/")
    async findAllParties(): Promise<IPart[]> {
        return await this.partiesService.findAllParties();
    }

    @httpGet("/:id")
    async findOnePart(@requestParam("id") partId: number): Promise<IPart> {
        return await this.partiesService.findPart(partId)
    }
}