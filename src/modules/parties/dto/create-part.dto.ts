import {IsEmail, IsNotEmpty, IsPhoneNumber} from "class-validator";

export class CreatePartDto {

    @IsNotEmpty()
    nome: string;

    @IsNotEmpty()
    sobrenome: string;

    @IsEmail()
    email: string;

    @IsNotEmpty()
    cpf: string;

    @IsPhoneNumber("BR")
    telefone: string;
}