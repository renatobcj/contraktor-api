import {inject, injectable} from "inversify";
import {PartiesRepository} from "./parties.repository";
import {IPart} from "./interfaces/part.interface";
import {CreatePartDto} from "./dto/create-part.dto";
import {isValidCpf} from "../../validators/cpf.validator";
import {SearchPartOptions} from "./interfaces/search-part.interface";

@injectable()
export class PartiesService {
    constructor(
        @inject("PartiesRepository") private partiesRepository: PartiesRepository
    ) {
    }

    async createPart(createPartDto: CreatePartDto): Promise<IPart> {
        const isValidCPF = isValidCpf(createPartDto.cpf);
        if (!isValidCPF) throw new Error("CPF inválido");

        const searchByCPF = await this.findAllParties({cpf: isValidCPF});
        if (searchByCPF.length > 0) throw new Error("Já existe uma parte cadastrada com esse CPF");

        createPartDto.cpf = isValidCPF;
        const createdPartId = await this.partiesRepository.createPart(createPartDto);
        return await this.findPart(createdPartId);
    }

    async findPart(partId: number): Promise<IPart> {
        return await this.partiesRepository.findPart(partId);
    }

    async findAllParties(filterOptions?: SearchPartOptions): Promise<IPart[]> {
        return await this.partiesRepository.findAllParties(filterOptions);
    }

    async findPartiesByContractId(contractId: number): Promise<IPart[]> {
        return await this.partiesRepository.findPartiesByContractId(contractId);
    }
}