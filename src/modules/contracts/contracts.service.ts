import {inject, injectable} from "inversify";
import {ContractsRepository} from "./contracts.repository";
import {IContract} from "./interfaces/contract.interface";
import {CreateContractDto} from "./dto/createContract.dto";
import {FilesService} from "../files/files.service";
import {IFile} from "../files/interfaces/file.interface";
import {ContractPartDto} from "./dto/contractPart.dto";
import {IPart} from "../parties/interfaces/part.interface";
import {PartiesService} from "../parties/parties.service";

@injectable()
export class ContractsService {
    constructor(
        @inject("FilesService") private filesService: FilesService,
        @inject("PartiesService") private partiesService: PartiesService,
        @inject("ContractsRepository") private contractsRepository: ContractsRepository,
    ) {
    }

    async createContract(contractFile: Express.Multer.File, createContractDto: CreateContractDto): Promise<IContract> {

        //adicionaria verificao das datas de vigencia aqui, por exemplo se a vigencia_fim pode ser no passado, etc...

        const createdFile: IFile = await this.filesService.createFile(contractFile);
        createContractDto.arquivo_id = createdFile.id;
        createContractDto.vigencia_inicio = createContractDto.vigencia_inicio.split("T")[0];
        createContractDto.vigencia_fim = createContractDto.vigencia_fim.split("T")[0];

        const createdContractId = await this.contractsRepository.createContract(createContractDto);
        return await this.findContract(createdContractId);
    }

    async findAllContracts(): Promise<IContract[]> {
        const contracts = await this.contractsRepository.findAllContracts();
        if (contracts.length === 0) return contracts;

        await Promise.all(contracts.map(async (contract: IContract, i) => {
            contracts[i].arquivo = await this.filesService.findFileByContractId(contract.id);
            contracts[i].partes = await this.partiesService.findPartiesByContractId(contract.id);
        }));

        return contracts;
    }

    async findContract(contractId: number): Promise<IContract> {
        const contract = await this.contractsRepository.findContract(contractId);
        if (!contract) throw new Error("Contrato não encontrado");

        contract.arquivo = await this.filesService.findFileByContractId(contract.id);
        contract.partes = await this.partiesService.findPartiesByContractId(contract.id);

        return contract;
    }

    async deleteContract(contractId: number): Promise<boolean> {
        await this.contractsRepository.deleteContract(contractId);
        return true;
    }

    async addPartToContract(contractPartDto: ContractPartDto): Promise<boolean> {
        const contract = await this.findContract(contractPartDto.contrato_id);

        const alreadyPart = contract.partes.filter((parte: IPart) => parte.id === contractPartDto.parte_id);
        if (alreadyPart.length > 0) throw new Error("Já é uma parte do contrato");

        const partInfo = await this.partiesService.findPart(contractPartDto.parte_id);
        if (!partInfo) throw new Error("Parte não cadastrada");

        await this.contractsRepository.addPartToContract(contractPartDto);
        return true;
    }

    async removePartFromContract(contractPartDto: ContractPartDto) {
        await this.contractsRepository.removePartFromContract(contractPartDto);
        return true;
    }
}
