import * as express from "express";
import {
    interfaces,
    controller,
    httpGet,
    httpPost,
    httpDelete,
    request,
    response,
    requestParam,
    requestBody, httpPut, httpPatch,
} from "inversify-express-utils";
import {inject} from "inversify";
import {ContractsService} from "./contracts.service";
import {IContract} from "./interfaces/contract.interface";
import {CreateContractDto} from "./dto/createContract.dto";
import {uploadMiddleware} from "../../middlewares/uploadFile.middleware";
import {validateDto} from "../../validators/dto.validator";
import {ContractPartDto} from "./dto/contractPart.dto";

@controller("/contracts")
export class ContractsController implements interfaces.Controller {
    constructor(
        @inject("ContractsService") private contractsService: ContractsService
    ) {
    }

    @httpPost("/", uploadMiddleware.single('file'))
    private async create(@request() req: express.Request, @requestBody() createContractDto: CreateContractDto): Promise<IContract> {
        const contractFile: Express.Multer.File = req.file;
        createContractDto = await validateDto(createContractDto, CreateContractDto);

        return await this.contractsService.createContract(contractFile, createContractDto);
    }

    @httpGet("/")
    private async findAll(): Promise<IContract[]> {
        return await this.contractsService.findAllContracts();
    }

    @httpGet("/:id")
    private async findOne(@requestParam("id") contractId: number): Promise<IContract> {
        return await this.contractsService.findContract(contractId);
    }

    @httpDelete("/:id")
    private async delete(@requestParam("id") contractId: number): Promise<boolean> {
        return await this.contractsService.deleteContract(contractId);
    }

    @httpPost("/part")
    private async addPartToContract(@requestBody() contractPartDto: ContractPartDto): Promise<Boolean> {
        contractPartDto = await validateDto(contractPartDto, ContractPartDto);
        return await this.contractsService.addPartToContract(contractPartDto);
    }

    @httpPatch("/part")
    private async removePartFromContract(@requestBody() contractPartDto: ContractPartDto): Promise<boolean> {
        contractPartDto = await validateDto(contractPartDto, ContractPartDto);
        return await this.contractsService.removePartFromContract(contractPartDto);
    }
}