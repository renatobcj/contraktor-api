import {IsNotEmpty, IsNumber} from "class-validator";

export class ContractPartDto {
    @IsNotEmpty()
    @IsNumber()
    contrato_id: number;

    @IsNotEmpty()
    @IsNumber()
    parte_id: number;
}