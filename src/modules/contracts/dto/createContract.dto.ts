import {IsDateString, IsNotEmpty, IsOptional, MaxLength} from "class-validator";

export class CreateContractDto {
    // as mensagens de erro das condicoes podem ser customizadas

    @IsNotEmpty()
    @MaxLength(50)
    titulo: string;

    @IsNotEmpty()
    @IsDateString()
    vigencia_inicio: string;

    @IsNotEmpty()
    @IsDateString()
    vigencia_fim: string;

    @IsOptional()
    arquivo_id: number
}
