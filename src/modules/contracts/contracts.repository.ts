import {inject, injectable} from "inversify";
import * as Knex from "knex";
import {IContract} from "./interfaces/contract.interface";
import {CreateContractDto} from "./dto/createContract.dto";
import {ContractPartDto} from "./dto/contractPart.dto";

@injectable()
export class ContractsRepository {
    constructor(@inject("DBConnection") private db: Knex) {
    }

    async createContract(createContractDto: CreateContractDto): Promise<number> {
        return await this.db('contratos').insert(createContractDto);
    }

    async deleteContract(contractId: number) {
        await this.db("contratos_partes").where({contrato_id: contractId}).del();
        return await this.db("contratos").where({id: contractId}).del();
    }

    async findAllContracts(): Promise<IContract[]> {
        return await this.db('contratos').select("*");
    }

    async findContract(contractId: number): Promise<IContract> {
        return await this.db("contratos").where({id: contractId}).select("*").first();
    }

    async addPartToContract(contractPartDto: ContractPartDto): Promise<number> {
        return await this.db("contratos_partes").insert(contractPartDto);
    }

    async removePartFromContract(contractPartDto: ContractPartDto): Promise<number> {
        return await this.db("contratos_partes")
            .where({parte_id: contractPartDto.parte_id, contrato_id: contractPartDto.contrato_id})
            .del();
    }
}