import {IFile} from "../../files/interfaces/file.interface";
import {IPart} from "../../parties/interfaces/part.interface";

export interface IContract {
    id: number;
    titulo: string;
    vigencia_inicio: string;
    vigencia_fim: string;
    arquivo_id: number;
    criado_em: Date;
    arquivo?: IFile
    partes?: IPart[]
}
