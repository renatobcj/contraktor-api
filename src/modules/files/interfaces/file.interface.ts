export interface IFile {
    id: number;
    tipo: string;
    arquivo: string;
    bytes: number;
    criado_em: Date;
}

export interface UploadFile {
    tipo: string,
    arquivo: string,
    bytes: number,
}