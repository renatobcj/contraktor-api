import {inject, injectable} from "inversify";
import * as Knex from "knex";
import {IFile, UploadFile} from "./interfaces/file.interface";

@injectable()
export class FilesRepository {
    constructor(@inject("DBConnection") private db: Knex) {
    }

    async createFile(file: UploadFile) {
        return await this.db("arquivos")
            .insert(file);
    }

    async findFile(where: object): Promise<IFile> {
        return await this.db("arquivos")
            .where(where)
            .select("*")
            .first();
    }

    async findFileByContractId(contractId: number): Promise<IFile> {
        return await this.db('arquivos')
            .leftJoin('contratos', 'arquivos.id', 'contratos.arquivo_id')
            .where('contratos.id', contractId)
            .select('arquivos.*')
            .first();

    }
}