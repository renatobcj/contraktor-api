import {inject, injectable} from "inversify";
import {IFile, UploadFile} from "./interfaces/file.interface";
import {FilesRepository} from "./files.repository";

@injectable()
export class FilesService {
    constructor(
        @inject("FilesRepository") private filesRepository: FilesRepository
    ) {
    }

    async createFile(file: Express.Multer.File): Promise<IFile> {

        const FILE_TYPES = {
            "application/pdf": "PDF",
            "application/msword": "DOC"
        };

        // formata os tipos de arquivos aceitos em uma string
        const acceptedTypes = Object.keys(FILE_TYPES).map(key => FILE_TYPES[key]).join(", ");
        const mimeType = file.mimetype;

        if (!(mimeType in FILE_TYPES))
            throw new Error(`Tipo de arquivo inválido, formatos aceitos: ${acceptedTypes}`);

        const fileToSave: UploadFile = {tipo: FILE_TYPES[mimeType], arquivo: file.path, bytes: file.size}
        const createdFileId = await this.filesRepository.createFile(fileToSave);

        return await this.filesRepository.findFile({id: createdFileId});
    }

    async findFileByContractId(contractId: number): Promise<IFile> {
        return await this.filesRepository.findFileByContractId(contractId);
    }
}