import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.raw("CREATE TABLE `partes` ( `id` INT(11) NOT NULL AUTO_INCREMENT, `nome` VARCHAR(50) NULL COLLATE 'latin1_swedish_ci', `sobrenome` VARCHAR(50) NULL COLLATE 'latin1_swedish_ci', `email` VARCHAR(100) NULL COLLATE 'latin1_swedish_ci', `cpf` VARCHAR(15) NULL COLLATE 'latin1_swedish_ci', `telefone` VARCHAR(15) NULL COLLATE 'latin1_swedish_ci', `criado_em` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP, PRIMARY KEY (`id`) USING BTREE ) COLLATE='latin1_swedish_ci' ENGINE=InnoDB AUTO_INCREMENT=17 ;")
}


export async function down(knex: Knex): Promise<void> {
    knex.raw("DROP TABLE `partes`;")
}

