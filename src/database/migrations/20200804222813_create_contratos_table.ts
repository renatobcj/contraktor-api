import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.raw("CREATE TABLE `contratos` ( `id` INT(11) NOT NULL AUTO_INCREMENT, `titulo` VARCHAR(100) NULL COLLATE 'latin1_swedish_ci', `vigencia_inicio` DATE NULL, `vigencia_fim` DATE NULL, `arquivo_id` INT(11) NULL, `criado_em` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP, PRIMARY KEY (`id`) USING BTREE ) COLLATE='latin1_swedish_ci' ENGINE=InnoDB AUTO_INCREMENT=17 ;")
}


export async function down(knex: Knex): Promise<void> {
    knex.raw("DROP TABLE `contratos`;")
}

