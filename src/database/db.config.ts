import * as Knex from 'knex';

const knexConfig = require('../../knexfile');
let knex: Knex = Knex(knexConfig);

export default knex;